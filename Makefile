######################################################################
#Makefile for Chris's FrankenCode which reposesses both Glass and
#Oculus routines to anaylse the field
######################################################################
OBJS=global.o m3dc1h.o bfield.o sawglass.o

###############################################################################################################################################################
FC=mpif90

 
#Fortran flags. -r8: reals are 8 bytes long mcmodel: need longer
#pointers to put code beyond 2G of memory
#-O3: compiler opimization. Moar takes longer but faster code
#-m64: Seems to be a C flag for compiling to 64 bit architectures
#-fno-alias: fortran no alias. Aliassing (several pointers referring to
#the same location) should not be assumed anywhere. 
#-ip: interprocedural optimizations. (inline function expansions, for functions in the same file
#-traceback: adds extra info to result so errors can be traced back.
#commented section is debugging implemented in a weird way (-debug doesn't have attribute full?

FFLAGS=-r8 -mcmodel=large -O3 -m64 -unroll0 -fno-alias -ip -traceback -check all -debug full -D DEBUG


###############################################################################################################################################################

 NAGFLAGS=-L${NAG_ROOT}/lib -lnag_nag

 HDF5FLAGS=-I$(HDF5_HOME)/include -L$(HDF5_HOME)/lib -lhdf5hl_fortran -lhdf5_hl -lhdf5_fortran -lhdf5 -lpthread -lz -lm

 M3DFLAGS=-L$(FIO_PATH)/lib -lfusionio -lm3dc1 -lstdc++ -Wl,-rpath,$(FIO_PATH)/lib/ -I$(FIO_PATH)/include

 OCFLAGS=-L$(OCULUS) -loculus -I$(OCULUS)


sawglass: $(OBJS)  
	$(FC) -o sawglass $(OBJS) $(OCFLAGS) $(NAGFLAGS) $(M3DFLAGS) $(HDF5FLAGS)
	date


######################################################################

global.o: global.F90 
	$(FC)  -o global.o -c global.F90 $(OCFLAGS) $(FFLAGS)

m3dc1h.o: m3dc1h.F90 global.o  
	$(FC)  -o m3dc1h.o -c m3dc1h.F90     $(FFLAGS)                  $(M3DFLAGS)

bfield.o: bfield.F90 global.o  
	$(FC)  -o bfield.o -c bfield.F90 $(FFLAGS) $(OCFLAGS) $(HDF5FLAGS) $(M3DFLAGS)

sawglass.o: sawglass.F90 global.o  
	$(FC) -o sawglass.o -c sawglass.F90 $(FFLAGS) $(OCFLAGS)

clean:
	rm $(OBJS) sawglass *.mod

cleann:
	rm $(OBJS) sawglass *.mod *.dat *.png 
  

