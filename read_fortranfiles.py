####################################################################################################
# read_fortranfiles

from scipy.io import FortranFile
import numpy as np
import glob as glob
import re  # regular expressions FTW!



def read_axisinfo(axisinfofilename):
    axisinfofile = FortranFile(axisinfofilename, 'r')
    numax = axisinfofile.read_ints()[0]
    axisinfo_array = []
    for i in range(numax):
        try:
            axisinfo_array.append(axisinfofile.read_reals())
        except TypeError:
            break
    return axisinfo_array

def readPointIntime(dir='./', pointType='opoint'):
    """
    reads all the opointnnn.dat files and creates a list of fixpt objects, each
    corresponding to a opointnnn.dat file.

    KeywordArguments:

    *dir*
        the directory where glob searches for opoint.dat or xpoint.dat files

    *pointType*
        opoint or xpoint (or possibly other types that fit the pattern pointTypeXXX.dat
    """
    def natural_key(string_):  # natural number sorting key
        """See http://www.codinghorror.com/blog/archives/001018.html"""
        return [int(s) if s.isdigit() else s for s in re.split(r'(\d+)', string_)]

    files = sorted(glob.glob(dir+pointType+'*.dat'), key=natural_key)
    list_of_fixpts = []
    for file in files:
        list_of_fixpts.append(fixpt(file))

    return list_of_fixpts

def Ellipticityfn(tg):
    return (1. - 0.25*((tg[0,0] + tg[1,1])**2.)) / (0.5*(tg[0,1]**2 + \
            tg[1,0]**2 + 0.5*((tg[0,0]-tg[1,1])**2.) )) #greene (measure preserving) eq. 9



class fixpt:
    """
    reads the information from the unformatted binary file created by sawglass.F90.
    Stores all the information in numpy arrays within the fixpt object.
    """
    def __init__(self, filename='NONE'):
        self.time     = np.empty((0, 1), dtype=np.int8)
        self.RZ       = np.empty((0, 2))
        self.iota     = np.empty((0, 1))
        self.realiota = np.empty((0, 1))
        self.tg       = np.empty((0, 2, 2))
        self.range    = (0,0)
        self.numel= 0
        if not filename == 'NONE':
            f = FortranFile(filename, 'r')
            try:
                while True:  # appending to numpy arrays, ugly slow and not very pythonic!
                    self.time = np.append(self.time, f.read_ints())
                    self.RZ = np.append(self.RZ, np.array([np.append(f.read_reals(), f.read_reals())]), axis=0)
                    iota = f.read_reals()
                    self.iota = np.append(self.iota, iota)
                    tg = np.array([f.read_reals().reshape(2, 2)])
                    self.tg = np.append(self.tg, tg , axis=0)
                    self.realiota = np.append(self.realiota, 1. - iota * np.sign(tg[0,0,1]))
                    self.numel += 1
            except TypeError:
               pass
            self.range = (self.time[0],self.time[-1])
            self.filename=filename


class axesCollection:
    """
    reads the information from the unformatted binary file created by sawglass.F90.
    Stores all the information in numpy arrays within the fixpt object.
    """
    def __init__(self, filename='NONE'):
        self.index     = np.empty((0, 1), dtype=np.int8)
        self.RZ       = np.empty((0, 2))
        self.iota     = np.empty((0, 1))
        self.realiota = np.empty((0, 1))
        self.tg       = np.empty((0, 2, 2))
        self.numel=0
        if not filename == 'NONE':
            f = FortranFile(filename, 'r')
            try:
                while True:  # appending to numpy arrays, ugly slow and not very pythonic!
                    self.index = np.append(self.index, f.read_ints())
                    self.RZ = np.append(self.RZ, np.array([np.append(f.read_reals(), f.read_reals())]), axis=0)
                    iota = f.read_reals()
                    self.iota = np.append(self.iota, iota)
                    tg = np.array([f.read_reals().reshape(2, 2)])
                    self.tg = np.append(self.tg, tg , axis=0)
                    self.realiota = np.append(self.realiota, 1. - iota * np.sign(tg[0,0,1]))
                    self.numel += 1
            except TypeError:
                pass
            self.filename=filename








