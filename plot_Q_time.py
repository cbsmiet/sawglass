####################################################################################################
#
#plot_Q_time.py: written by csmiet (started 17 september 2018)


import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from scipy.io import FortranFile
import subprocess
import matplotlib.pyplot as plt
import matplotlib
from matplotlib import rc
import matplotlib.gridspec as gridspec

rc('text', usetex = True)
matplotlib.rcParams['savefig.dpi'] = '72'
matplotlib.rcParams['xtick.labelsize'] = 15
matplotlib.rcParams['ytick.labelsize'] = 15
matplotlib.rcParams['xtick.major.size'] = 2.5
matplotlib.rcParams['xtick.direction'] = 'in'
matplotlib.rcParams['ytick.major.size'] = 2.5
matplotlib.rcParams['ytick.direction'] = 'in'


def read_axisinfo(axisinfofilename):
    axisinfofile = FortranFile(axisinfofilename, 'r')
    numax = axisinfofile.read_ints()[0]
    axisinfo_array = []
    for i in range(numax):
        try:
            axisinfo_array.append(axisinfofile.read_reals())
        except TypeError:
            break
    return axisinfo_array

array_of_axisinfos = []

start=50
stop=175

transformarray=np.empty((stop-start,2))
transformarray.fill(np.nan)
j=0


for i in range(start, stop):
    print(i)
    filename = 'out' + str(i) + '_axes_residues.dat'
    axisinfo=read_axisinfo(filename)
    transformarray[j,0]=axisinfo[0][0]
    if len(axisinfo)>1:
        transformarray[j,1]=axisinfo[1][0]
    j = j+1



fig = plt.figure()
fig.set_size_inches(15,12)
plt.ylabel(r'$q_0$', fontsize=25)
plt.xlabel(r'$t$', fontsize=25)

plt.plot(range(0,stop-start), transformarray[:,0], c='b', marker='x', label=r'main axis')
plt.plot(range(0,stop-start), transformarray[:,1], c='g', marker='x', label=r'secondary axis')
plt.show()



