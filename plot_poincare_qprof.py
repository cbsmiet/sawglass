####################################################################################################
#
#plot_poincare_qprof.py: written by csmiet (started 14 september 2018)


import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from scipy.io import FortranFile
import subprocess
from matplotlib import rc
import matplotlib.gridspec as gridspec
import glob as glob
import read_fortranfiles as rf

rc('text', usetex = True)
rc('font', family='serif')
matplotlib.rcParams['savefig.dpi'] = '72'
matplotlib.rcParams['xtick.labelsize'] = 15
matplotlib.rcParams['ytick.labelsize'] = 15
matplotlib.rcParams['xtick.major.size'] = 2.5
matplotlib.rcParams['xtick.direction'] = 'in'
matplotlib.rcParams['ytick.major.size'] = 2.5
matplotlib.rcParams['ytick.direction'] = 'in'



ppts = 1000
ptrj = 100
Ra = 3.2
Za = 0
Poincare_line_length=1.2


for i in range(0, 1000):
    timename = 'out'+str(i)

    fig = plt.figure()
    fig.set_size_inches(15,12)
    gs = gridspec.GridSpec(2, 2, height_ratios=[3.5,1], width_ratios=[7,1])


    ax1 = plt.subplot(gs[0,0])
    ax2 = plt.subplot(gs[1,0])
    colorbar_grid = gridspec.GridSpecFromSubplotSpec(1, 2, subplot_spec=gs[:, 1])
    ax_cb1 = plt.subplot(colorbar_grid[0])
    ax_cb1.set_axis_off()
    ax_cb2 = plt.subplot(colorbar_grid[1])
    ax_cb2.set_axis_off()


    cbar1pos = ax_cb1.get_position()
    cbar1newpos = [cbar1pos.x0 , cbar1pos.y0,  cbar1pos.width / 3.0, cbar1pos.height ]#thin the cbaraxes
    ax_cb1=fig.add_axes(cbar1newpos)

    cbar2pos = ax_cb2.get_position()
    cbar2newpos = [cbar2pos.x0 , cbar2pos.y0,  cbar2pos.width / 3.0, cbar2pos.height ]#thin the cbaraxes
    ax_cb2=fig.add_axes(cbar2newpos)

    ppointfile = FortranFile(timename+'_ppoints.dat', 'r')
    twistfile = FortranFile(timename+'_protations.dat', 'r')
    twists = twistfile.read_reals()
    qvalues = 1.0/np.abs(twists)
    axislabelfile = FortranFile(timename+'_axislabel.dat', 'r')
    axislabel = axislabelfile.read_ints()
    RZlinefile = FortranFile(timename+'_RZline.dat', 'r')
    RZline = RZlinefile.read_reals().reshape(2,-1)


    norm1 = matplotlib.colors.Normalize(vmin=.7, vmax=1.3)
    cmap1 = matplotlib.cm.get_cmap('RdYlBu')
    s_mDiv = matplotlib.cm.ScalarMappable(cmap=cmap1, norm=norm1)
    s_mDiv.set_array([])

    norm2 = matplotlib.colors.Normalize(vmin=0.9, vmax=1.1)
    cmap2 = matplotlib.cm.get_cmap('Greens')
    s_mGn = matplotlib.cm.ScalarMappable(cmap=cmap2, norm=norm2)
    s_mGn.set_array([])

    norm3 = matplotlib.colors.Normalize(vmin=0.7, vmax=1.1)
    cmap3 = matplotlib.cm.get_cmap('Reds')
    s_mRd = matplotlib.cm.ScalarMappable(cmap=cmap3, norm=norm3)
    s_mRd.set_array([])
    print(axislabel)
    for j in range(1, ptrj):
        # print(j, twists[j])
        ppoints = ppointfile.read_reals().reshape(-1,2).swapaxes(0,1)
        if abs(abs(twists[j]) - 1.0) < 0.0001:
            # print('plotting surface close to 1')
            ax1.scatter(ppoints[0,:], ppoints[1,:], color='k' ,s=0.2)
        elif axislabel[j] == 2:
            # print('plotting axis2')
            ax1.scatter(ppoints[0,:], ppoints[1,:], color=s_mGn.to_rgba(qvalues[j]),s=0.2)
        elif abs(twists[j]) > 1.0:  #therefore q is smaller than 1
            # print('plotting inner axis1')
            ax1.scatter(ppoints[0,:], ppoints[1,:], color=s_mDiv.to_rgba(qvalues[j]),s=0.2)
        elif abs(twists[j]) < 1.0:
            # print('plotting outer axis1')
            ax1.scatter(ppoints[0,:], ppoints[1,:], color=s_mDiv.to_rgba(qvalues[j]),s=0.2)

    opoints = rf.axesCollection(timename+'_opoints.dat')
    xpoints = rf.axesCollection(timename+'_xpoints.dat')
    mainpoints = rf.axesCollection(timename+'_mainpoints.dat')
    ax1.scatter(opoints.RZ[:, 0], opoints.RZ[:, 1], s=30, facecolors='w', edgecolors='b', linewidth=1, zorder=10)
    ax1.scatter(xpoints.RZ[:, 0], xpoints.RZ[:, 1], s=40, marker='x', c='m', zorder=10)
    print('should have '+ str(xpoints.numel)+' xpoints!')
    print(mainpoints.RZ)

    axesfile = FortranFile(timename+'_axes.dat', 'r')
    axes = axesfile.read_reals().reshape(2,-1)
    fixptsfile = FortranFile(timename+'_fixpts.dat', 'r')
    fixpts = fixptsfile.read_reals().reshape(2,-1)



    for j in range(mainpoints.numel):
        ax1.scatter(mainpoints.RZ[j,0], mainpoints.RZ[j,1], s=40, facecolors='w', edgecolors='r', linewidth=1, zorder=10)



    # Plot the line along which the poincare section was calculated
    ax1.plot(RZline[0,:], RZline[1,:], color='k')

    ax1.set_xlabel(r'$R$', fontsize=25)
    ax1.set_ylabel(r'$Z$', fontsize=25)
    cbar = plt.colorbar(s_mGn, cax= ax_cb2)
    cbar2 = plt.colorbar(s_mDiv, cax=ax_cb1)
    cbar.ax.set_ylabel(r'$q$', fontsize=25)
    ax1.set_aspect('equal')
    ax1.set_xlim(2.0, 4.2)
    ax1.set_ylim(-.9, .9)
    fname = 'poincare'+str(i)+'.png'

    Ra = axes[0, 0]  # update the location of the main axis
    Za = axes[1, 0]

    colorray = [['b','g'][i-1] for i in axislabel]  # if axislabel is 1 it becomes  'b'
    ax2.scatter(np.linspace(0, Poincare_line_length, 100), qvalues, color=colorray)  # Remember this is incorrect since 0 is not included as a poincare points seecd!
    ax2.scatter(0, 1/mainpoints.realiota[0], s=40, facecolors='w', edgecolors='r', linewidth=1, zorder=10)
    if mainpoints.numel > 1:
        print((mainpoints.RZ[0]-mainpoints.RZ[1])**2)
        secondPointDistance=np.sqrt(np.sum((mainpoints.RZ[0]-mainpoints.RZ[1])**2))
        ax2.scatter(secondPointDistance, 1/mainpoints.realiota[1], s=40, facecolors='w', edgecolors='r', linewidth=1, zorder=10)
    ax2.set_xlabel(r'$|\mathbf{x}_0 -\mathbf{x}|$', fontsize=25)
    ax2.set_ylabel(r'$|q|$', fontsize=25)
    ax2.set_xlim(xmin=0, xmax=1.2)
    ax2.set_ylim(ymin=0.8, ymax=1.5)
    ax2.axhline(1.0, c='c')
#    plt.xlim(xmin=0, xmax=2.2)
#    plt.ylim(ymin=.7, ymax=5.9)

    filename = 'fullfig'+str(i)+'.png'
    fig.savefig(filename, bbox_inches='tight')
    print("done with timestep ", i)
