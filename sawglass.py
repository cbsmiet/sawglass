####################################################################################################
#
# sawglass.py: written by csmiet 23 march 2018
# writes a .gl file with input parameters and then calls
# the sawglass executable which reads it and writes out
# files with poincarepoints, axes and rotational transforms.
# Then this script plots it beautifully.
# another version of this script might do this using multiprocessing in python
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from scipy.io import FortranFile
import subprocess
import matplotlib.pyplot as plt
import matplotlib
from matplotlib import rc

rc('text', usetex = True)
matplotlib.rcParams['savefig.dpi'] = '72'
matplotlib.rcParams['xtick.labelsize'] = 15
matplotlib.rcParams['ytick.labelsize'] = 15
matplotlib.rcParams['xtick.major.size'] = 2.5
matplotlib.rcParams['xtick.direction'] = 'in'
matplotlib.rcParams['ytick.major.size'] = 2.5
matplotlib.rcParams['ytick.direction'] = 'in'

def writesettings(fname, c1file= '/p/m3dc1/csmiet/case1/C1.h5',outfile = 'out',timestep = 40, ppts = 1000,
        ptrj = 100, grid_Rmin = 2.8, grid_Rmax = 3.6, grid_Zmin= -.5,
        grid_Zmax = .5, Poincare_line_length = 0.7, grid_dim=50, Ra=3.2, Za=0.0):
    axesfile = open(fname+'.gl', 'w')
    axesfile.write('&glassin \n')
    axesfile.write('        c1file   = \'{:>25}'.format(c1file) + '\',\n')
    axesfile.write('        outfile  = \'{:>25}'.format(outfile) + '\',\n')
    axesfile.write('        timestep = {:>25}'.format(int(timestep)) + ',\n')
    axesfile.write('            ppts = {:>25}'.format(int(ppts)) + ',\n')
    axesfile.write('            ptrj = {:>25}'.format(int(ptrj)) + ',\n')
    axesfile.write('       grid_Rmin = {:>25f}'.format(grid_Rmin) + ',\n')
    axesfile.write('       grid_Zmin = {:>25f}'.format(grid_Zmin) + ',\n')
    axesfile.write('       grid_Rmax = {:>25f}'.format(grid_Rmax) + ',\n')
    axesfile.write('       grid_Zmax = {:>25f}'.format(grid_Zmax) + ',\n')
    axesfile.write('       grid_dim  = {:>25f}'.format(grid_dim) + ',\n')
    axesfile.write('       Poincare_line_length = {:>25f}'.format(Poincare_line_length) + ',\n')
    axesfile.write('              Ra = {:>25f}'.format(Ra) + ',\n')
    axesfile.write('              Za = {:>25f}'.format(Za) + ',\n')
    axesfile.write('/\n')
    axesfile.close()
    return

def read_axisinfo(axisinfofilename):
    axisinfofile = FortranFile(axisinfofilename, 'r')
    numax = axisinfofile.read_ints()[0]
    axisinfo_array = []
    for i in range(numax):
        axisinfo_array.append(axisinfofile.read_reals())
    return axisinfo_array

nproc = 64
ppts = 1000
ptrj = 100
Ra = 3.2
Za = 0
Poincare_line_length=1.2


for i in range(0, 225):
    timename = 'out'+str(i)
    writesettings('Fri', ppts=ppts, ptrj=ptrj, timestep=i, outfile=timename,
                  Poincare_line_length=Poincare_line_length, Ra=Ra, Za=Za)
    sawglass = subprocess.Popen(['mpirun', '-np', str(nproc), './sawglass', 'Fri'])  # run sawglass
    sawglass.wait()

    fig1 = plt.figure(1)
    fig1.set_size_inches(17,12)

    ppointfile = FortranFile(timename+'_ppoints.dat', 'r')
    twistfile = FortranFile(timename+'_protations.dat', 'r')
    twists = twistfile.read_reals()
    qvalues = 1.0/np.abs(twists)
    axislabelfile = FortranFile(timename+'_axislabel.dat', 'r')
    axislabel = axislabelfile.read_ints()
    RZlinefile = FortranFile(timename+'_RZline.dat', 'r')
    RZline = RZlinefile.read_reals().reshape(2,-1)


    norm1 = matplotlib.colors.Normalize(vmin=.7, vmax=1.3)
    cmap1 = matplotlib.cm.get_cmap('RdYlBu')
    s_mDiv = matplotlib.cm.ScalarMappable(cmap=cmap1, norm=norm1)
    s_mDiv.set_array([])

    norm2 = matplotlib.colors.Normalize(vmin=0.9, vmax=1.1)
    cmap2 = matplotlib.cm.get_cmap('Greens')
    s_mGn = matplotlib.cm.ScalarMappable(cmap=cmap2, norm=norm2)
    s_mGn.set_array([])

    norm3 = matplotlib.colors.Normalize(vmin=0.7, vmax=1.1)
    cmap3 = matplotlib.cm.get_cmap('Reds')
    s_mRd = matplotlib.cm.ScalarMappable(cmap=cmap3, norm=norm3)
    s_mRd.set_array([])
    #print(axislabel)
    for j in range(1, ptrj):
        # print(j, twists[j])
        ppoints = ppointfile.read_reals().reshape(-1,2).swapaxes(0,1)
        if abs(abs(twists[j]) - 1.0) < 0.01:
            # print('plotting surface close to 1')
            plt.scatter(ppoints[0,:], ppoints[1,:], color='k' ,s=0.2)
        elif axislabel[j] == 2:
            # print('plotting axis2')
            plt.scatter(ppoints[0,:], ppoints[1,:], color=s_mGn.to_rgba(qvalues[j]),s=0.2)
        elif abs(twists[j]) > 1.0:  #therefore q is smaller than 1
            # print('plotting inner axis1')
            plt.scatter(ppoints[0,:], ppoints[1,:], color=s_mDiv.to_rgba(qvalues[j]),s=0.2)
        elif abs(twists[j]) < 1.0:
            # print('plotting outer axis1')
            plt.scatter(ppoints[0,:], ppoints[1,:], color=s_mDiv.to_rgba(qvalues[j]),s=0.2)
    axesfile = FortranFile(timename+'_axes.dat', 'r')
    axes = axesfile.read_reals().reshape(2,-1)
    fixptsfile = FortranFile(timename+'_fixpts.dat', 'r')
    fixpts = fixptsfile.read_reals().reshape(2,-1)

    # Always plot the first axis! (it always exists)
    plt.scatter(axes[0, 0], axes[1, 0], s=40, facecolors='w', edgecolors='r', linewidth=1, zorder=10)
    if len(axes[0,:]) > 1:  # there is more than one axis found
        plt.scatter(axes[0,1], axes[1,1], s=40, facecolors='w', edgecolors='b', linewidth=1, zorder=10)


    plt.scatter(fixpts[0, :], fixpts[1, :], color='k')

    # Plot the line along which the poincare section was calculated
    #print(RZline)
    plt.plot(RZline[0,:], RZline[1,:], color='k')

    plt.xlabel(r'$R$', fontsize=25)
    plt.ylabel(r'$Z$', fontsize=25)
    cbar = plt.colorbar(s_mRd)
    cbar2 = plt.colorbar(s_mDiv)
    cbar.ax.set_ylabel(r'$q$', fontsize=25)
    plt.gca().set_aspect('equal')
    plt.gca().set_xlim(2.0, 4.2)
    plt.gca().set_ylim(-.9, .9)
    fname = 'poincare'+str(i)+'.png'
    fig1.savefig(fname, bbox_inches='tight')

#    Ra = axes[0, 0]  # change the location of the main axis
#    Za = axes[1, 0]

    plt.close()
    fig2 = plt.figure(2)
    fig2.set_size_inches(10, 6)
    colorray = [['b','g'][i-1] for i in axislabel]  # if axislabel is 1 it becomes  'b'
    plt.scatter(np.linspace(0, Poincare_line_length, 100), qvalues, color=colorray)  # Remember this is incorrect since 0 is not included as a poincare points seecd!
    qprofname = 'qprof'+str(i)+'.png'
    plt.xlabel(r'some line $R$', fontsize=25)
    plt.ylabel(r'$|q|$', fontsize=25)
    plt.gca().set_xlim(xmin=0, xmax=1.2)
    plt.gca().set_ylim(ymin=0.7, ymax=2.5)
#    plt.xlim(xmin=0, xmax=2.2)
#    plt.ylim(ymin=.7, ymax=5.9)
    fig2.savefig(qprofname, bbox_inches='tight')
    plt.close()
    print("done with timestep ", i)

#    rzffile = FortranFile('ga00aarzf.dat', 'r')
#    rzf = rzffile.read_reals().reshape(-1, 3)
#    plt.scatter(rzf[:,0], rzf[:,1], color='k')
#    for j in range(0,len(rzf[:,0])-1):
#        plt.gca().arrow(rzf[j, 0], rzf[j, 1], rzf[j+1, 0]-rzf[j, 0], rzf[j+1, 1]-rzf[j, 1], head_width=0.005, head_length=0.01, fc='k', ec='k')

