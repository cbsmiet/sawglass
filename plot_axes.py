####################################################################################################
#
#plot_axespy: written by csmiet (started 18 December 2018)


import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from scipy.io import FortranFile
import subprocess
from matplotlib import rc
import matplotlib.gridspec as gridspec
import glob as glob
import read_fortranfiles as rf

rc('text', usetex = True)
rc('font', family='serif')
matplotlib.rcParams['savefig.dpi'] = '72'
matplotlib.rcParams['xtick.labelsize'] = 15
matplotlib.rcParams['ytick.labelsize'] = 15
matplotlib.rcParams['xtick.major.size'] = 2.5
matplotlib.rcParams['xtick.direction'] = 'in'
matplotlib.rcParams['ytick.major.size'] = 2.5
matplotlib.rcParams['ytick.direction'] = 'in'
matplotlib.rcParams["figure.figsize"] = [6.4, 4.8]


fig = plt.figure()
files = glob.glob('opoint*')
for file in files:
    a = rf.fixpt(file)
    plt.plot(a.time, 1/a.realiota)


plt.xlabel(r'time')
plt.ylabel(r'$|q_{\rm ax}|$')
plt.xlim(81, 193)
plt.ylim(0.9, 1.02)
plt.hlines(1., 0, 1000, colors='xkcd:light grey', zorder=0)
plt.savefig('q_axis.png', bbox_inches='tight')
plt.show()
