import pylab as plt
from scipy.io import FortranFile

nppoints=100



g=FortranFile('ppoints.dat', 'r')
for i in range(nppoints):
  print(i)
  c=g.read_reals().reshape(-1,2).swapaxes(0,1)
  plt.scatter(c[0,:], c[1,:], color='k' ,s=1)

f = FortranFile('axes.dat', 'r')
b=f.read_reals().reshape(2,-1)
plt.scatter(b[0,:],b[1,:], color='r')

plt.xlim((2.5, 3.9))

plt.show()

