subroutine al00aa(settingsfile)
  !-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!
  use constants !??
  use glass
  use mpi
  !-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!
  implicit none
  LOGICAL    :: exist
  integer    :: ierr
  real       :: X02AJF
  character, intent(in) :: settingsfile*100 
  !-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!
  machprec = X02AJF() ; vsmall = 10*machprec ; small = 100*vsmall ; sqrtmachprec = sqrt(machprec) ! returns machine precision;
  if( myid.eq.0 ) then ! only the master node reads the input; 25 Mar 15;
    write(ounit,'("al00aa  : ", 10x ," : ext = ",a," ;")') trim(settingsfile)
    inquire( file=trim(settingsfile)//".gl", exist=exist )
    if( exist ) then 
      open( iunit, file=trim(settingsfile)//".gl", status="unknown" )
      read( iunit, glassin )
      close(iunit)
    else !throw fatal exception 
      if( .true. ) then
        write(0,'("al00aa :      fatal : myid=",i3," ; .true. ; input file not provided  ;")') myid
        call MPI_ABORT( MPI_COMM_WORLD, 1, ierr )
        stop      "al00aa :            :          ; .true. : input file not provided  ;"
      endif
    endif ! matches if( exist ) ; 01 Oct 13; 
  endif ! end of if( myid.eq.0 ) ; 25 Mar 15;
  !-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!
  call MPI_BCAST(ext      ,100 ,MPI_CHARACTER,0 ,MPI_COMM_WORLD,ierr)
  call MPI_BCAST(timestep ,1 ,MPI_INTEGER,0 ,MPI_COMM_WORLD,ierr)
  call MPI_BCAST(Ra       ,1 ,MPI_DOUBLE_PRECISION,0 ,MPI_COMM_WORLD,ierr)
  call MPI_BCAST(Za       ,1 ,MPI_DOUBLE_PRECISION,0 ,MPI_COMM_WORLD,ierr)
  call MPI_BCAST(Ppts     ,1 ,MPI_INTEGER,0 ,MPI_COMM_WORLD,ierr)
  call MPI_BCAST(Ptrj     ,1 ,MPI_INTEGER,0 ,MPI_COMM_WORLD,ierr)
  call MPI_BCAST(Rl       ,1 ,MPI_DOUBLE_PRECISION,0 ,MPI_COMM_WORLD,ierr)
  call MPI_BCAST(Zl       ,1 ,MPI_DOUBLE_PRECISION,0 ,MPI_COMM_WORLD,ierr)
  call MPI_BCAST(Ru       ,1 ,MPI_DOUBLE_PRECISION,0 ,MPI_COMM_WORLD,ierr)
  call MPI_BCAST(Zu       ,1 ,MPI_DOUBLE_PRECISION,0 ,MPI_COMM_WORLD,ierr)
  !-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!
  return
end subroutine al00aa
!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!
